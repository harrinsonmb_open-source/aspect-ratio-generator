const postcss = require('postcss')
const fs = require('fs');


const output = postcss()
  .use(require('postcss-aspect-ratio-polyfill'))
  .process(require('fs').readFileSync('input.css', 'utf8'))
  .css

fs.writeFile('output.css', output, function (err) {
  if (err) return console.log(err);
  console.log('file created!');
});
