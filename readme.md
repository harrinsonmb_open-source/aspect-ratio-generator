# aspect-ratio-generator
This script is kind of a polyfill for the "aspect-ratio" css property .

## Usage
1. Execute npm install.
2. Place your css as input.css inside this project folder.
3. Execute npm start.
4. That's all, now you will see you new output.css file created.